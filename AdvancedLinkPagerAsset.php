<?php

namespace azbuco\advancedlinkpager;

use yii\web\AssetBundle;

class AdvancedLinkPagerAsset extends AssetBundle {

    public $sourcePath = '@azbuco/advancedlinkpager/assets';
    public $css = [
        'css/advanced-link-pager.css',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

}
