<?php

namespace azbuco\advancedlinkpager;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Inflector;

class AdvancedLinkPager extends Widget
{
    /**
     * @var Pagination the pagination object that this pager is associated with.
     * You must set this property in order to make LinkPager work.
     */
    public $pagination;

    /**
     * @var boolean whether to register link tags in the HTML header for prev, next, first and last page.
     * Defaults to `false` to avoid conflicts when multiple pagers are used on one page.
     * @see http://www.w3.org/TR/html401/struct/links.html#h-12.1.2
     * @see registerLinkTags()
     */
    public $registerLinkTags = false;

    /**
     * @var array the list of page sizes
     */
    public $pageSizes = [10, 20, 50, 100, 200];

    /**
     * @var array HTML attributes for the pager container tag.
     */
    public $options = ['class' => 'pagination advanced-link-pager'];

    /**
     * @var array HTML attributes for the input group tag.
     */
    public $inputGroupOptions = ['class' => 'input-group input-group-sm'];

    /**
     * @var array HTML attributes for the size tag.
     */
    public $sizeOptions = ['class' => 'form-control'];

    /**
     * @var array HTML attributes for the page tag.
     */
    public $pageOptions = ['class' => 'form-control'];
    public $layout = '{pagination} {pagesize}';

    /**
     * Initializes the pager.
     */
    public function init()
    {
        if ($this->pagination === null) {
            throw new InvalidConfigException('The "pagination" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Executes the widget.
     * This overrides the parent implementation by displaying the generated page buttons.
     */
    public function run()
    {
        if ($this->registerLinkTags) {
            $this->registerLinkTags();
        }

        echo $this->renderSections();
        $this->registerBundle();
        $this->registerScript();
    }

    /**
     * Registers relational link tags in the html header for prev, next, first and last page.
     * These links are generated using [[\yii\data\Pagination::getLinks()]].
     * @see http://www.w3.org/TR/html401/struct/links.html#h-12.1.2
     */
    protected function registerLinkTags()
    {
        $view = $this->getView();
        foreach ($this->pagination->getLinks() as $rel => $href) {
            $view->registerLinkTag(['rel' => $rel, 'href' => $href], $rel);
        }
    }

    protected function renderSections()
    {
        $content = preg_replace_callback("/{(\\w+)}/", function ($matches) {
            $renderMethod = 'render' . Inflector::id2camel($matches[1]);
            if (!method_exists($this, $renderMethod)) {
                return $matches[0];
            }
            return $this->$renderMethod();
        }, $this->layout);

        return '<div ' . Html::renderTagAttributes($this->options) . '>' . $content . '</div>';
    }

    protected function renderPagination()
    {
        $pages = $this->paginationDropdown();
        $groupAttributes = Html::renderTagAttributes($this->inputGroupOptions);

        return <<<EOT
<div $groupAttributes>
    <span class="input-group-addon">
        Ugrás a(z)
    </span>    
       $pages 
    <span class="input-group-addon">
        oldalra
    </span>
</div>
EOT;
    }

    protected function paginationDropdown()
    {
        $selection = $this->pagination->createUrl($this->pagination->getPage());

        $items = [];
        $pageCount = $this->pagination->getPageCount();

        for ($i = 0; $i < $pageCount; $i++) {
            $items[$this->pagination->createUrl($i)] = $i + 1;
        }

        return Html::dropDownList('', $selection, $items, $this->pageOptions);
    }

    protected function renderPagesize()
    {
        $perPages = $this->perPagesDropdown();
        $groupAttributes = Html::renderTagAttributes($this->inputGroupOptions);

        return <<<EOT
<div $groupAttributes>            
    $perPages
    <span class="input-group-addon">
        elem / oldal
    </span>
</div>
EOT;
    }

    protected function perPagesDropdown()
    {
        $items = [];
        foreach ($this->pageSizes as $size) {
            $items[$this->pagination->createUrl($this->pagination->getPage(), $size)] = $size;
        }

        return Html::dropDownList(
                $this->pagination->pageSizeParam, $this->pagination->createUrl($this->pagination->getPage(), $this->pagination->pageSize), $items, $this->sizeOptions
        );
    }

    public function registerBundle()
    {
        AdvancedLinkPagerAsset::register($this->view);
    }

    public function registerScript()
    {
        $id = $this->options['id'];

        $js = '; $("#' . $id . ' select").change(function(){ window.location.href = $(this).val(); });';

        $this->view->registerJs($js);
    }
}
